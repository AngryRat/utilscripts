#reads a G Suite/Google Workspace upload user file (csv) with First Name and Last Name filled in
#makes the required Email Address
#Password, Org Unit Path, and Change Password at Next Sign-in is left to the user to fill in (but doing it should be trivial with a spreadsheet)

DELIM=","
FNAMEPOS=0
LNAMEPOS=1
EMAILPOS=2
skipheader=-1
MUSTSKIP=-1

infilename=input("enter input file\n")
infile=open(infilename,'r')
outfilename=input("enter output file\n")
outfile=open(outfilename,'w')
domain=input("enter Google domain\n")

for line in infile:
#{
    if (skipheader==MUSTSKIP): #skip the header
    #{
        skipheader=42
        outfile.write(line)
    #}
    else:
    #{
        t=line.strip()
        l=t.split(DELIM)
        fname=l[FNAMEPOS].split() #first and/or last names may be multiple words
        lname=l[LNAMEPOS].split() #so get all words into username
        username=""
        for name in fname:
        #{
            username=username+(name.lower())
        #} finished gluing first name
        for name in lname:
        #{
            username=username+(name.lower())
        #} finished gluing last name
        username=username+"@"+domain
        l[EMAILPOS]=username
        l[FNAMEPOS]=l[FNAMEPOS].strip() #get rid of trailing whitespace from a
        l[LNAMEPOS]=l[LNAMEPOS].strip() #bad copypaste
        outline=DELIM.join(l)
        outfile.write((outline+"\n"))
    #}
#} end for line in infile

infile.close()
outfile.close()

