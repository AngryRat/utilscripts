#run this at the root of the directories that need fixing
#root's permissions should already be fixed (this script will not touch it)
#this script goes into all files in all subdirectories and sets file permissions

#useful when they are lost (ex. files restored from external drive formatted to
#a non-Unix filessystem such as FAT32)

import subprocess
import os
import os.path

dirPerms="755"
filePerms="644"
CURRENTDIR=0
SUBDIRS=1
FILEZ=2

homu=os.getcwd()
tour=os.walk(os.getcwd())

for stop in tour:
#{
    for subd in stop[SUBDIRS]:
    #{ fix perms of all subdirs in current dir
        fullsubdpath=os.path.join(stop[CURRENTDIR],subd)
        #print(fullsubdpath+" is a subdirectory")
        subprocess.run(["chmod",dirPerms,fullsubdpath]) #chmod 755 subdir
    #} end subdir fixing
    for aFile in stop[FILEZ]:
    #{ fix perms of all files in current dir
        fullfilepath=os.path.join(stop[CURRENTDIR],aFile)
        #print(fullfilepath+" is a file")
        subprocess.run(["chmod",filePerms,fullfilepath]) #chmod 644 file
    #} end file fixing
#} #tour complete

