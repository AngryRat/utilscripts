#reads a file that contains notes on all files in the current dir
#input file has the format filename<whitespace>-notes where notes will indicate
#whether or not is useless

#if the file has a note that shows it is useless ignore it
#if not useless move the file to the parent directory
import subprocess

MUDA="USELESS"

infilename=input("enter filename\n")
infile=open(infilename,'r')

for line in infile:
#{
    if (MUDA in line):
        continue
    else:
    #{
        t=line.strip()
        goodline=t.split()
        subprocess.run(["mv",goodline[0],".."]) #run 'mv filename ..'
    #} end else (file is not useless)
#} end for line in infile

infile.close()
